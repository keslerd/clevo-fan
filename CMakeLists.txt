cmake_minimum_required (VERSION 3.0)
project(ClevoFanControl VERSION 0.1.0)

set(INSTALL_BIN_DIR "${CMAKE_INSTALL_PREFIX}/bin" CACHE PATH "Installation directory for executables")
# Skip adding rpath when linking a shared library
set (CMAKE_SKIP_RPATH NO)
# Skip adding rpath when installing the targets
set (CMAKE_SKIP_INSTALL_RPATH YES)

include_directories("${PROJECT_BINARY_DIR}")

set(SOURCES
    "${PROJECT_SOURCE_DIR}/src/main.cpp"

    "${PROJECT_SOURCE_DIR}/src/clevo.cpp"
	)

add_compile_options(-Wall -Wextra)

add_executable(clevo-fan-control ${SOURCES})
set_property(TARGET clevo-fan-control PROPERTY CXX_STANDARD 14)
set_property(TARGET clevo-fan-control PROPERTY CXX_STANDARD_REQUIRED ON)
##
## Install targets
##
install (TARGETS clevo-fan-control DESTINATION "${INSTALL_BIN_DIR}")
