#include "clevo.hpp"
#include "P641HK1.h"

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/io.h>
#include <sys/mman.h>
#include <sys/prctl.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

Clevo::Clevo() {
    init();
}

bool Clevo::init() {
    if (ioperm(EC_DATA, 1, 1) != 0)
        return EXIT_FAILURE;
    if (ioperm(EC_SC, 1, 1) != 0)
        return EXIT_FAILURE;
    return EXIT_SUCCESS;
}

int Clevo::io_wait(const uint32_t port, const uint32_t flag, const char value) {
    uint8_t data = inb(port);
    int i = 0;
    while ((((data >> flag) & 0x1) != value) && (i++ < 100)) {
        usleep(1000);
        data = inb(port);
    }
    if (i >= 1000) {
        printf("wait_ec error on port 0x%x, data=0x%x, flag=0x%x, value=0x%x\n",
                port, data, flag, value);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

uint8_t Clevo::io_read(const uint32_t port) {
    io_wait(EC_SC, IBF, 0);
    outb(EC_SC_READ_CMD, EC_SC);

    io_wait(EC_SC, IBF, 0);
    outb(port, EC_DATA);

    //wait_ec(EC_SC, EC_SC_IBF_FREE);
    io_wait(EC_SC, OBF, 1);
    uint8_t value = inb(EC_DATA);

    return value;
}

int Clevo::io_do(const uint32_t cmd, const uint32_t port, const uint8_t value) {
    io_wait(EC_SC, IBF, 0);
    outb(cmd, EC_SC);

    io_wait(EC_SC, IBF, 0);
    outb(port, EC_DATA);

    io_wait(EC_SC, IBF, 0);
    outb(value, EC_DATA);

    return io_wait(EC_SC, IBF, 0);
}

int Clevo::getCpuTemp(void) {
    return io_read(EC_REG_CPU_TEMP);
}

int Clevo::getGpuTemp(void) {
    return io_read(EC_REG_GPU_TEMP);
}

int Clevo::getCpuFanDuty(void) {
    int raw_duty = io_read(EC_REG_CPU_FAN_DUTY);
    return calculateFanDuty(raw_duty);
}

int Clevo::getCpuFanRpms(void) {
    int raw_rpm_hi = io_read(EC_REG_CPU_FAN_RPMS_HI);
    int raw_rpm_lo = io_read(EC_REG_CPU_FAN_RPMS_LO);
    return calculateFanRpms(raw_rpm_hi, raw_rpm_lo);
}

int Clevo::getGpuFanDuty(void) {
    int raw_duty = io_read(EC_REG_GPU_FAN_DUTY);
     return calculateFanDuty(raw_duty);
}

int Clevo::getGpuFanRpms(void) {
    int raw_rpm_hi = io_read(EC_REG_GPU_FAN_RPMS_HI);
    int raw_rpm_lo = io_read(EC_REG_GPU_FAN_RPMS_LO);
    return calculateFanRpms(raw_rpm_hi, raw_rpm_lo);
}

int Clevo::setCpuFanDuty(int duty_percentage) {
    if (duty_percentage < 0 || duty_percentage > 100) {
        printf("Wrong fan duty to write: %d\n", duty_percentage);
        return EXIT_FAILURE;
    }
    double v_d = ((double) duty_percentage) / 100.0 * 255.0 + 0.5;
    int v_i = (int) v_d;
    return io_do(0x99, 0x01, v_i);
}

int Clevo::setGpuFanDuty(int duty_percentage) {
    if (duty_percentage < 0 || duty_percentage > 100) {
        printf("Wrong fan duty to write: %d\n", duty_percentage);
        return EXIT_FAILURE;
    }
    double v_d = ((double) duty_percentage) / 100.0 * 255.0 + 0.5;
    int v_i = (int) v_d;
    return io_do(0x99, 0x02, v_i);
}

int Clevo::calculateFanDuty(int raw_duty) {
    return (int) ((double) raw_duty / 255.0 * 100.0 + 0.5);
}

int Clevo::calculateFanRpms(int raw_rpm_high, int raw_rpm_low) {
    int raw_rpm = (raw_rpm_high << 8) + raw_rpm_low;
    return raw_rpm > 0 ? (2156220 / raw_rpm) : 0;
}
