#include "clevo.hpp"
#include <iostream>
#include <chrono>
#include <thread>

using namespace std::chrono_literals;

int main(int argc, char const* argv[])
{
    Clevo clevo;

    unsigned temp_duty_map[][3] = {
        // {temp, cpu_duty, gpu_duty}
        {0,   0, 0},
        {30,  0, 0},
        {50, 40, 0},
        {55, 50, 40},
        {60, 50, 50},
        {65, 60, 60},
        {70, 80, 80},
        {75, 100, 100}
    };

    //clevo.setGpuFanDuty(100);
    int old_temp = 0;
    while(1) {
        auto cpu_temp = clevo.getCpuTemp();
        // auto gpu_temp = clevo.getGpuTemp();
        auto cpu_duty = clevo.getCpuFanDuty();
        auto gpu_duty = clevo.getGpuFanDuty();

        if(cpu_temp != old_temp) {
            int tgt_cpu_duty = 0;
            int tgt_gpu_duty = 0;

            for(auto tm: temp_duty_map) {
                if(cpu_temp >= tm[0]) {
                    tgt_cpu_duty = tm[1];
                    tgt_gpu_duty = tm[2];
                }
            }

            if(tgt_cpu_duty != cpu_duty)
                clevo.setCpuFanDuty(tgt_cpu_duty);

            if(tgt_gpu_duty != gpu_duty)
                clevo.setGpuFanDuty(tgt_gpu_duty);

            std::cout << "CPU[temp]: " << cpu_temp << "\n";
            std::cout << "CPU[ fan]: " << cpu_duty << "\n";
            // std::cout << "GPU[temp]: " << gpu_temp << "\n";
            std::cout << "GPU[ fan]: " << gpu_duty << "\n";
            std::cout << std::endl;

            old_temp = cpu_temp;
        }

        std::this_thread::sleep_for(1s);
    }

    return 0;
}
