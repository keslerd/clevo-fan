#ifndef CLEVO_H
#define CLEVO_H

#include <stdint.h>

int ec_init(void);
int ec_auto_duty_adjust(void);
int ec_query_cpu_temp(void);
int ec_query_gpu_temp(void);
int ec_query_cpu_fan_duty(void);
int ec_query_cpu_fan_rpms(void);
int ec_query_gpu_fan_duty(void);
int ec_query_gpu_fan_rpms(void);
int ec_write_cpu_fan_duty(int duty_percentage);
int ec_write_gpu_fan_duty(int duty_percentage);
int ec_io_wait(const uint32_t port, const uint32_t flag,
        const char value);
uint8_t ec_io_read(const uint32_t port);
int ec_io_do(const uint32_t cmd, const uint32_t port,
        const uint8_t value);
int calculate_fan_duty(int raw_duty);
int calculate_fan_rpms(int raw_rpm_high, int raw_rpm_low);
int check_proc_instances(const char* proc_name);
//static void get_time_string(char* buffer, size_t max, const char* format);

#endif /* CLEVO_H */
