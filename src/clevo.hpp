#ifndef CLEVO_HPP
#define CLEVO_HPP

#include <cstdint>

class Clevo {
public:
    Clevo();

    bool init();
    int io_wait(const uint32_t port, const uint32_t flag, const char value);
    int io_do(const uint32_t cmd, const uint32_t port, const uint8_t value);
    uint8_t io_read(const uint32_t port);

    int getCpuTemp();
    int getGpuTemp();

    int getCpuFanDuty();
    int getCpuFanRpms();
    int getGpuFanDuty();
    int getGpuFanRpms();

    int setCpuFanDuty(int duty_percentage);
    int setGpuFanDuty(int duty_percentage);

private:
    int calculateFanDuty(int raw_duty);
    int calculateFanRpms(int raw_rpm_high, int raw_rpm_low);
};

#endif /* CLEVO_HPP */
